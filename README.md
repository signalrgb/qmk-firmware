# SignalRGB QMK Repo Has Moved
[Now on Github](https://github.com/SRGBmods/QMK)

[Documentation](https://docs.signalrgb.com/qmk/building-firmware-from-source)